package com.nimus;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Sean on 6/7/2015.
 */
public class ImageAnalyzer {
    //returns color variation, mean R, mean G, mean B
    public static float[] analyzeColor(BufferedImage image, float radius) {
        float[] hsb = new float[3];
        float meanHue = 0;
        float meanSqHue = 0;
        float meanR = 0;
        float meanG = 0;
        float meanB = 0;
        float n = 0;
        float centerX = image.getWidth() / 2.0f;
        float centerY = image.getHeight() / 2.0f;

        for (int x = 0; x < image.getWidth(); x++) {
            for (int y = 0; y < image.getHeight(); y++) {
                if (Math.sqrt(Math.pow(centerX - x, 2.0) + Math.pow(centerY - y, 2.0)) < radius) {
                    int rgb = image.getRGB(x, y);

                    int r = (rgb >> 16) & 0x000000FF;
                    int g = (rgb >> 8) & 0x000000FF;
                    int b = (rgb) & 0x000000FF;

                    meanR += r;
                    meanG += g;
                    meanB += b;

                    Color.RGBtoHSB(r, g, b, hsb);

                    meanHue += hsb[0];
                    meanSqHue += hsb[0] * hsb[0];
                    n++;
                }
            }
        }

        meanHue /= n;
        meanR /= n;
        meanG /= n;
        meanB /= n;

        float varHue = meanSqHue / n - meanHue * meanHue;

        return new float[] {varHue, meanR, meanG, meanB};
    }

    public static float analyzeComplexity(BufferedImage image, BufferedImage edges, float radius) {
        float diff = 0;
        float centerX = image.getWidth() / 2.0f;
        float centerY = image.getHeight() / 2.0f;

        for (int x = 1; x < image.getWidth() - 1; x++) {
            for (int y = 1; y < image.getHeight() - 1; y++) {
                if (Math.sqrt(Math.pow(centerX - x, 2.0) + Math.pow(centerY - y, 2.0)) < radius) {
                    float d = getGrayF(image, x, y);

                    for (int iX = -1; iX <= 1; iX++) {
                        for (int iY = -1; iY <= 1; iY++) {
                            if (!(iX == 0 && iY == 0)) {
                                d -= 1.0 / 8.0 * getGrayF(image, x + iX, y + iY);
                            }
                        }
                    }

                    d = (float) Math.pow(d, 2.0);

                    d *= 255.0f * 255.0f;

                    diff += d;

                    int rgb = (255 << 24) | ((int) d << 16) | ((int) d << 8) | (int) d;

                    if (edges != null) {
                        edges.setRGB(x, y, rgb);
                    }
                } else {
                    if (edges != null) {
                        edges.setRGB(x, y, image.getRGB(x, y));
                    }
                }
            }
        }

        return diff / 100000.0f;
    }

    public static float getGrayF(BufferedImage image, int x, int y) {
        int rgb = image.getRGB(x, y);

        //int a = (rgb >> 24) & 0x000000FF;
        int r = (rgb >> 16) & 0x000000FF;
        int g = (rgb >> 8) & 0x000000FF;
        int b = (rgb) & 0x000000FF;
        int f = (int) (0.2126f * r + 0.7152f * g + 0.0722f * b);

        return f / 255.0f;
    }

    public static float getR(BufferedImage image, int x, int y) {
        int rgb = image.getRGB(x, y);

        return ((rgb >> 16) & 0x000000FF) / 255.0f;
    }
}
