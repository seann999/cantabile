package com.nimus;

import netP5.NetAddress;
import oscP5.OscMessage;
import oscP5.OscP5;

/**
 * Created by Sean on 6/17/2015.
 */
public class MidiCom {
    OscP5 oscP5;
    NetAddress myRemoteLocation;

    public MidiCom() {
        oscP5 = new OscP5(this,12000);

        myRemoteLocation = new NetAddress("127.0.0.1",11000);
    }

    public void noteOn(int pitch) {
        OscMessage myMessage = new OscMessage("/msg");

        myMessage.add(pitch);

        oscP5.send(myMessage, myRemoteLocation);


    }
}
