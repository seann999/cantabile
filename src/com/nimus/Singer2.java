package com.nimus;

import net.beadsproject.beads.core.AudioContext;
import net.beadsproject.beads.data.Sample;
import net.beadsproject.beads.ugens.Gain;
import net.beadsproject.beads.ugens.Glide;
import net.beadsproject.beads.ugens.SamplePlayer;

import java.text.DecimalFormat;

/**
 * Created by Sean on 6/20/2015.
 */
public class Singer2 extends Singer {

    private AudioContext ac;
    private float[] phonemes = new float[] {0f, 0.4f};

    public Singer2() {
        ac = new AudioContext();

        Tools.normalize(phonemes);

        try {
            DecimalFormat df = new DecimalFormat("00");
            for (int i = 0; i < 28; i++) {
                String num = df.format((i + 1));
                String filename = "assets/vocal2/Aaah " + num + " " + Tools.getKeyOnly(i - 3) + ".wav";
                SamplePlayer player = new SamplePlayer(ac, new Sample(filename));
                player.setKillOnEnd(false);
                players.add(player);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void init() {
        for (int i = 0; i < players.size(); i++) {
            SamplePlayer player = players.get(i);

            Glide gainValue = new Glide(ac, 1, 200);
            gainValues.add(gainValue);
            Gain gain = new Gain(ac, 1, gainValue);
            gain.addInput(player);
            ac.out.addInput(gain);

            player.setToEnd();
            player.setLoopType(SamplePlayer.LoopType.NO_LOOP_FORWARDS);
            player.start();
        }

        ac.start();
    }

    public void singKey(int key, int duration) {
        //key = key + pitchOffset;

        int x = Tools.weightedRandom(phonemes);
        int i = (key - 33);//33 to 61
        SamplePlayer player = players.get(i);
        Glide gainValue = gainValues.get(i);
        gainValue.setValueImmediately(1);
        gainValue.setGlideTime(duration);
        player.setToLoopStart();
        gainValue.setValue(0f);
    }
}
