package com.nimus;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamResolution;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by Sean on 6/7/2015.
 */
public class CantabileFrame extends JFrame {
    private Webcam webcam;

    public static Process PYTHON;
    public static PrintWriter PYTHONOUTPUT;

    public CantabileFrame() {
        super("Cantabile");

        System.out.println("initializing...");

        loadCamera();

        CameraPanel panel = new CameraPanel(webcam, new Conductor2(new Singer1()));

        setContentPane(panel);

        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setVisible(true);
        setLocationRelativeTo(null);

//        String[] cmd={"python",
//                "C:/Users/Sean/Anaconda/Scripts/pizzaTest.py"};
//
//        try {
//            PYTHON = Runtime.getRuntime().exec(cmd);
//            PYTHONOUTPUT = new PrintWriter(new OutputStreamWriter(PYTHON.getOutputStream()));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                try {
                    CameraPanel.STOP = true;
                    PYTHONOUTPUT.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }

                if (PYTHON != null) {
                    PYTHON.destroy();
                }
            }
        });

        System.out.println("initialized.");
    }

    private void loadCamera() {
        List<Webcam> webcams = Webcam.getWebcams();
        webcam = Webcam.getDefault();

        for (Webcam cam : webcams) {
            if (cam.getName().toLowerCase().contains("droid")) {
                webcam = cam;
            }
        }

        webcam.setViewSize(WebcamResolution.VGA.getSize());
        webcam.open();
    }
}
