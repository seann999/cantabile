package com.nimus;

import java.awt.*;
import java.util.Arrays;

/**
 * Created by Sean on 6/9/2015.
 */
public class Tools {
    public final static String[] NOTES = new String[88];

    static {
        NOTES[0] = "A";
        NOTES[1] = "A#";
        NOTES[2] = "B";
        NOTES[3] = "C";
        NOTES[4] = "C#";
        NOTES[5] = "D";
        NOTES[6] = "D#";
        NOTES[7] = "E";
        NOTES[8] = "F";
        NOTES[9] = "F#";
        NOTES[10] = "G";
        NOTES[11] = "G#";
    }

    public static String getKeyOnly(int i) {
        int x = i - 1;
        if (x < 0) {
            x += 12;
        }
        x = x % 12;
        return NOTES[x];
    }

    public static String getKey(int i) {
        int x = i - 1;
        if (x < 0) {
            x += 12;
        }
        int oct = (int) ((i + 8.0) / 12.0);
        x = x % 12;
        return NOTES[x] + oct;
    }

    public static float map(float value,
                     float istart,
                     float istop,
                     float ostart,
                     float ostop) {
        return ostart + (ostop - ostart) * ((value - istart) / (istop - istart));
    }

    public static float[] normalize(float[] dist) {
        float sum = 0;

        for (int y = 0; y < dist.length; y++) {
            sum += dist[y];
        }

        if (sum != 0) {
            for (int y = 0; y < dist.length; y++) {
                dist[y] /= (sum);// + 1);
            }
        }

        return dist;
    }

    private static float[] DIST;

    public static float[] N(float mean, float sd, int length) {
        if (DIST == null || DIST.length != length) {
            DIST = new float[length];
        }

        for (int i = 0; i < length; i++) {
            DIST[i] = pN(mean, sd, i);
        }

        return DIST;
    }

    public static float pN(float mean, float sd, float x) {
        return (float) ((1.0 / (sd*Math.sqrt(2.0*Math.PI)))*Math.exp((-Math.pow((x-mean),2.0))/(2*Math.pow(sd,2.0))));
    }

    public static int weightedRandom(float[] dist) {
        float r = (float) Math.random();
        float cum = 0;

        for (int x = 0; x < dist.length; x++) {
            cum += dist[x];

            if (r < cum) {
                return x;
            }
        }

        return 0;
    }

    public static String rgbToString(int rgb) {
        int r = (rgb >> 16) & 0x000000FF;
        int g = (rgb >> 8) & 0x000000FF;
        int b = (rgb) & 0x000000FF;

        return r + " " + g + " " + b;
    }

    public static void rgbToHSB(float[] rgb, float[] hsb) {
        Color.RGBtoHSB((int) rgb[0], (int) rgb[1], (int) rgb[2], hsb);
    }

    private static float[] tmp = new float[3];

    public static float[] rgbToHSB(int rgb) {
        int r = (rgb >> 16) & 0x000000FF;
        int g = (rgb >> 8) & 0x000000FF;
        int b = (rgb) & 0x000000FF;

        Color.RGBtoHSB(r, g, b, tmp);

        return tmp;
    }

    public static String rgbToHSBString(int rgb) {
        return Arrays.toString(rgbToHSB(rgb));
    }
}
