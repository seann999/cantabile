package com.nimus.classifier;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.Arrays;

/**
 * Created by Sean on 6/27/2015.
 */
public class ColorClassifier implements KeyListener{
    //NULL MUST BE LAST
    public enum MyColor { YELLOW, GREEN, WHITE, BLACK, PINK, BROWN, RED, NULL};

    ColorClass[] classes = new ColorClass[MyColor.values().length];
    ColorClass current;
    int currentIndex = 0;

    public ColorClassifier() {
        for (int i = 0 ; i < classes.length; i++) {
            classes[i] = new ColorClass();
        }
    }

    public void addData(float[] hsb) {
        for (int i = 0; i < 3; i++) {
            current.sum[i] += hsb[i];
            current.sqsum[i] += hsb[i] * hsb[i];
            if (hsb[i] < current.min[i]) {
                current.min[i] = hsb[i];
            }

            if (hsb[i] > current.max[i]) {
                current.max[i] = hsb[i];
            }
        }

        current.n += 1;

        System.out.println("ADDING DATA: " + Arrays.toString(hsb));
    }

    public void finalize() {
        ColorClass colors = current;
        for (int i = 0; i < 3; i++) {
            colors.mean[i] = colors.sum[i] / colors.n;
            colors.sum[i] = 0;
            float variance = colors.sqsum[i] / colors.n - colors.mean[i] * colors.mean[i];
            colors.sqsum[i] = 0;
            colors.spread[i] = (float) (Math.sqrt(variance) * 2.0);
        }

        System.out.println("FINALIZED [" + MyColor.values()[currentIndex].name() + "] : ");
        System.out.println("-MEAN: " + Arrays.toString(colors.mean));
        System.out.println("-SPREAD: " + Arrays.toString(colors.spread));
        System.out.println("-MIN: " + Arrays.toString(colors.min));
        System.out.println("-MAX: " + Arrays.toString(colors.max));
        //System.out.println("-MIN/MAX: " + Arrays.toString(colors.spread));

        colors.n = 0;
    }

    Color brown = new Color(128, 128, 0);

    public Color getColorRGB(float h, float s, float b) {
        MyColor color = getColor(h, s, b);

        if (color == MyColor.RED) {
            return Color.RED;
        } else if (color == MyColor.GREEN) {
            return Color.GREEN;
        } else if (color == MyColor.YELLOW) {
            return Color.YELLOW;
        } else if (color == MyColor.WHITE) {
            return Color.WHITE;
        } else if (color == MyColor.BLACK) {
            return Color.BLACK;
        } else if (color == MyColor.PINK) {
            return Color.PINK;
        } else if (color == MyColor.BROWN) {
            return brown;
        } else {
            return Color.BLUE;
        }
    }

    public MyColor getColor(float[] hsb) {
        return MyColor.values()[(getColorI(hsb[0], hsb[1], hsb[2]))];
    }

    public MyColor getColor(float h, float s, float b) {
        return MyColor.values()[(getColorI(h, s, b))];
    }

    public int getColorI(float h, float s, float b) {
        for (int i = 0; i < classes.length - 1; i++) {
            if (classes[i].contains(h, s, b)) {
                return i;
            }
        }

        return classes.length - 1;
    }

    public String getColorName() {
        return MyColor.values()[currentIndex].name();
    }

    public void editSpread(float h, float s, float b) {

        current.spread[0] += h;
        current.spread[1] += s;
        current.spread[2] += b;
        current.spread[0] = Math.min(1, Math.max(0, current.spread[0]));
        current.spread[1] = Math.min(1,Math.max(0, current.spread[1]));
        current.spread[2] = Math.min(1,Math.max(0, current.spread[2]));

        System.out.println("SPREAD [" + getColorName() + "] : " + Arrays.toString(current.spread));
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        boolean caps = Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK);

        if (caps) {
            try {
                int i = Integer.parseInt(e.getKeyChar() + "");
                i--;

                System.out.println("MODE SET TO: " + MyColor.values()[i].name());

                currentIndex = i;
                current = classes[i];
            } catch (Exception ex) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    finalize();

                //} else if (e.getKeyChar() == 'P') {
                //    editSpread(0.1f, 0, 0);
                } else if (e.getKeyChar() == '[') {
                    editSpread(0f, 0.1f, 0);
                } else if (e.getKeyChar() == ']') {
                    editSpread(0.f, 0, 0.1f);
                } else if (e.getKeyChar() == 'p') {
                    editSpread(-0.1f, 0, 0);
                } else if (e.getKeyChar() == '{') {
                    editSpread(0.f, -0.1f, 0);
                } else if (e.getKeyChar() == '}') {
                    editSpread(0f, 0, -0.1f);
                } else if (e.getKeyChar() == '/') {
                    for (int i = 0; i < 3; i++) {
                        current.max[i] = 0;
                        current.min[i] = 1;
                    }
                } else if (e.getKeyChar() == ';') {
                    saveFile();
                } else if (e.getKeyChar() == 'P') {
                    loadFile();
                }
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    public void loadFile() {
        System.out.println("loaded file.");
        String filename = "assets/classData.txt";

        try {

            File fXmlFile = new File(filename);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            //optional, but recommended
            //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
            doc.getDocumentElement().normalize();

            NodeList nList = doc.getChildNodes().item(0).getChildNodes();

            for (int temp = 1; temp < nList.getLength(); temp += 2) {

                Node nNode = nList.item(temp);

                int index = Integer.parseInt(nNode.getNodeName().substring(1, 2));

                NodeList nL = nNode.getChildNodes();
                ColorClass cc = classes[index];

                for (int i = 1; i < nL.getLength(); i += 2) {
                    Element nNode2 = (Element) nL.item(i);
                    cc.mean[(i - 1) / 2] = Float.parseFloat(nNode2.getElementsByTagName("mean").
                            item(0).getTextContent());
                    cc.spread[(i - 1) / 2] = Float.parseFloat(nNode2.getElementsByTagName("spread").
                            item(0).getTextContent());
                    cc.min[(i - 1) / 2] = Float.parseFloat(nNode2.getElementsByTagName("min").
                            item(0).getTextContent());
                    cc.max[(i - 1) / 2] = Float.parseFloat(nNode2.getElementsByTagName("max").
                            item(0).getTextContent());
                }

//                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
//
//                    Element eElement = (Element) nNode;
//
//                    System.out.println("Staff id : " + eElement.getAttribute("id"));
//                    System.out.println("First Name : " + eElement.getElementsByTagName("firstname").item(0).getTextContent());
//                    System.out.println("Last Name : " + eElement.getElementsByTagName("lastname").item(0).getTextContent());
//                    System.out.println("Nick Name : " + eElement.getElementsByTagName("nickname").item(0).getTextContent());
//                    System.out.println("Salary : " + eElement.getElementsByTagName("salary").item(0).getTextContent());
//
//                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveFile() {
        String filename = "assets/classData.txt";
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();

            Element rootElement = doc.createElement("root");
            doc.appendChild(rootElement);

            for (int i = 0; i < classes.length; i++) {
                Element classElement = doc.createElement("C" + i);
                rootElement.appendChild(classElement);
                classes[i].appendData(doc, classElement);
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(filename));
            transformer.transform(source, result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("SAVED FILE.");
    }
}
