package com.nimus.classifier;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Created by Sean on 6/27/2015.
 */
public class ColorClass {
    float[] mean = new float[3];
    float[] spread = new float[3];
    float[] sum = new float[3];
    float[] sqsum = new float[3];
    float[] max = new float[3];
    float[] min = new float[3];
    float n;

    public ColorClass() {
        for (int i = 0; i < 3; i++) {
            spread[i] = 0f;
            max[i] = 0;
            min[i] = 1;
        }
    }

    public void appendData(Document doc, Element c) {
        for (int i = 0; i < 3; i++) {
            Element ic = doc.createElement("HSB" + i);
            c.appendChild(ic);

            Element meanE = doc.createElement("mean");
            meanE.appendChild(doc.createTextNode(mean[i] + ""));
            ic.appendChild(meanE);

            Element spreadE = doc.createElement("spread");
            spreadE.appendChild(doc.createTextNode(spread[i] + ""));
            ic.appendChild(spreadE);

            Element maxE = doc.createElement("max");
            maxE.appendChild(doc.createTextNode(max[i] + ""));
            ic.appendChild(maxE);

            Element minE = doc.createElement("min");
            minE.appendChild(doc.createTextNode(min[i] + ""));
            ic.appendChild(minE);
        }
    }

    public boolean contains(float h, float s, float b) {
        return c(h, 0) && c(s, 1) && c(b, 2);
    }

    private boolean c(float x, int i) {
//        if (x < mean[i] + spread[i] && x > mean[i] - spread[i]) {
//            return true;
//        }
        if (x <= max[i] && x >= min[i]) {
            return true;
        }

        return false;
    }
}
