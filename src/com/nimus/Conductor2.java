package com.nimus;

import com.nimus.classifier.ColorClassifier;
import com.nimus.classifier.ColorClassifier.MyColor;

import java.awt.*;
import java.awt.image.BufferedImage;


/**
 * Created by Sean on 6/20/2015.
 */
public class Conductor2 extends Conductor {

    CameraPanel cam;


    float bThres = 0.1f;
    float sThres = 0.5f;

    boolean process = true;
    Thread thread;

    MyColor currentColor = MyColor.NULL;

    boolean stop = false;
    ColorClassifier cc;

    public Conductor2(Singer singer) {
        this.singer = singer;
    }

    public void init(ColorClassifier cc) {
        this.cc = cc;
    }

    public void start(CameraPanel cam) {
        stop = false;
        this.cam = cam;
        this.cc = cc;

        float[] hsb = cam.needleHSB;
        //MyColor firstColor = cc.getColor(hsb[0], hsb[1], hsb[2]);
        currentColor = MyColor.NULL;

        singer.init();

        thread = new Thread() {
            public void run() {
                float variation = 0;
                singing = true;

                while (cam.timePercent < 0.9 || stop) {
                    //System.out.println(cam.timePercent);
                    float[] hsb = cam.needleHSB;

                    MyColor color = cc.getColor(hsb[0], hsb[1], hsb[2]);
                    int key = colorToKey(color);

                    //System.out.println("COLOR: " + color);

                    if (currentColor != color) {
                        CameraPanel.KEYCOLOR = cc.getColorRGB(hsb[0], hsb[1], hsb[2]);
                        if (key == -1) {
                            //singer.pause(50);
                        } else {
                            singer.pause(50);
                            CameraPanel.FIRE.sing();
                            System.out.println(key);
                            singer.singKey(key, 1000);
                            midi.noteOn(key);
                            variation++;
                        }
                    }

                    currentColor = color;
                }

                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                singer.pause(1000);
                singing = false;
                finished = true;

                try {
                    Thread.sleep(1500);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                System.out.println("variation: " + variation);
                float apVol = variation / 100.0f;
                apVol = Math.min(1.0f, apVol);
                System.out.println("applause volume: " + apVol);
                ((Singer1) singer).applaud(apVol);
            }
        };
        thread.start();
    }

 //   public void applaud() {
        //((Singer1) singer).applaud();
   // }
   // }

    public int colorToKey(MyColor color) {
        //39B,40C,44E,47G,51B,52C,56E
        if (color == MyColor.RED) {
            return 47;
        } else if (color == MyColor.GREEN) {
            return 44;
        } else if (color == MyColor.PINK) {
            return 52;
        } else if (color == MyColor.BROWN) {
            return 40;
        } else if (color == MyColor.YELLOW) {
            return 51;
        } else if (color == MyColor.WHITE) {
            return 56;
            //return -1;
        } else if (color == MyColor.BLACK) {
            return 39;
        } else {
            return -1;
        }
    }

    float[] hsb = new float[3];
    @Override
    public Color classifyColor(Color color) {
        Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), hsb);
        return cc.getColorRGB(hsb[0], hsb[1], hsb[2]);
    }



    public void keyPressed(char key) {
        if (key == 't') {
            bThres += 0.1;
        } else if (key == 'y') {
            bThres -= 0.1;
        } else if (key == 'g') {
            sThres += 0.1;
        } else if (key == 'h') {
            sThres -= 0.1;
        } else if (key == 'u' || key == 'U') {
            process = !process;
        }

      //  System.out.println("BT: " + bThres + " ST: " + sThres);
    }

    public void stop() {
        stop = true;
    }

    @Override
    public void processImage(BufferedImage image, float radius) {
        if (process) {
            float centerX = image.getWidth() / 2.0f;
            float centerY = image.getHeight() / 2.0f;
            float[] hsb = new float[3];

            for (int x = 1; x < image.getWidth() - 1; x++) {
                for (int y = 1; y < image.getHeight() - 1; y++) {
                    if (Math.sqrt(Math.pow(centerX - x, 2.0) + Math.pow(centerY - y, 2.0)) < radius) {
                        int rgb = image.getRGB(x, y);

                        int r = (rgb >> 16) & 0x000000FF;
                        int g = (rgb >> 8) & 0x000000FF;
                        int b = (rgb) & 0x000000FF;

                        Color.RGBtoHSB(r, g, b, hsb);

                        Color color = cc.getColorRGB(hsb[0], hsb[1], hsb[2]);

                        image.setRGB(x, y, color.getRGB());
                    }
                }
            }
        }
    }
}
