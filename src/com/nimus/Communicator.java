package com.nimus;

import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortList;

public class Communicator {
    private SerialPort serialPort;

    public Communicator() {
        String[] portNames = SerialPortList.getPortNames();
        for(int i = 0; i < portNames.length; i++){
            System.out.println(portNames[i]);
        }

        serialPort = new SerialPort("COM6");
        try {
            serialPort.openPort();//Open serial port
            serialPort.setParams(SerialPort.BAUDRATE_9600,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);//Set params. Also you can set params by this string: serialPort.setParams(9600, 8, 1, 0);

        }
        catch (SerialPortException ex) {
            System.out.println(ex);
        }
    }

    public SerialPort getSerial() {
        return serialPort;
    }

    public void close() {
        //new Exception().printStackTrace();
        try {
            serialPort.closePort();//Close serial port
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}