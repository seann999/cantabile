package com.nimus;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.Random;

/**
 * Created by Sean on 6/27/2015.
 */
public class Fire {
    public enum State {IDLE, IGNITE, ON, COOLDOWN}

    public State state = State.IDLE;
    private Random random = new Random();
    private float anim = 1;
    private boolean fire = false;
    private float add = 0;

    public Fire() {}

    public void sing() {
        /*add = 200;*/
    }

    public int getIntensity() {
        if (add > 0) {
            add -= 0.002;
        }
        float x = 0;
        float a = 195f;
        float b = 5f;
        float sd = 0;
        boolean caps = Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK);

        // System.out.println(state);
        if (state == State.ON || caps) {
            x = 100;
            sd = 50;
        } else if (state == State.IGNITE) {

            //float x = 0;
            if (anim <= 0.5) {
                x = (float) ((Math.sin(anim / 0.5f * Math.PI - Math.PI / 2.0f) + 1.0f) / 2.0f * a + b);
            } else {
                float t = (anim - 0.5f) / 0.5f;
                x = (float) ((a + b) - (Math.sin(t * Math.PI - Math.PI / 2.0f) + 1f) / 2f * 100f);
            }

            anim += 0.01;

            if (anim >= 1) {
                anim = 0;
                state = State.ON;

            }

            sd = 5 + 45f * anim;

            //System.out.println(x);

            // x += random.nextInt((int) (anim * 45.0f) + 5);

        } else if (state == State.IDLE) {
            x = 5;
            sd = 5;
        } else if (state == State.COOLDOWN) {

            float t = anim;
           // float x = 0;
            //x = 100f - t * 95f;
            x = (float) (100 - (Math.sin(t * Math.PI - Math.PI / 2.0f) + 1f) / 2f * 95f);
            //x += random.nextInt(50);
            //return (int) x;
            sd = 50 - 45f * anim;
            anim += 0.03;
            if (anim >= 1) {
                anim = 0;
                state = State.IDLE;
            }
        }

       // System.out.println("a" + add);
        return (int)Math.min(255,((x + Math.random() * sd) + add));
    }



    public void fireOn() {
        anim = 0;
        fire = true;
        state = State.IGNITE;
    }

    public void fireOff() {
        anim = 0;
        fire = false;
        state = State.COOLDOWN;
    }
}
