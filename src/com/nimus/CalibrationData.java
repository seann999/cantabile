package com.nimus;

/**
 * Created by Sean on 6/14/2015.
 */
public class CalibrationData {
    private float BGComplexitySum;
    public float BGComplexityMean;

    private float[] BGColorSum = new float[3];
    public float[] BGColorMean = new float[3];

    private float BGDataN;

    public void feed(float BGComplexity, float[] BGColor) {
        BGComplexitySum += BGComplexity;
        BGDataN += 1.0f;
        BGComplexityMean = BGComplexitySum / BGDataN;

        for (int i = 0; i < 3; i++) {
            BGColorSum[i] += BGColor[i];
            BGColorMean[i] = BGColorSum[i] / BGDataN;
        }
    }

    public void reset() {
        BGComplexitySum = 0;
        BGDataN = 0;

        for (int i = 0; i < 3; i++) {
            BGColorSum[i] = 0;
            BGColorMean[i] = 0;
        }
    }
}
