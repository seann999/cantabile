package com.nimus;

import java.awt.*;

/**
 * Created by Sean on 6/25/2015.
 */
public class Note {
    public float x, y;
    public int key;
    public Color color;

    public Note(float x, float y) {
        this.x = x;
        this.y = y;
    }
}
