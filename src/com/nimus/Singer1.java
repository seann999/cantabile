package com.nimus;

import net.beadsproject.beads.data.Sample;
import net.beadsproject.beads.ugens.Gain;
import net.beadsproject.beads.ugens.Glide;
import net.beadsproject.beads.ugens.SamplePlayer;

/**
 * Created by Sean on 6/20/2015.
 */
public class Singer1 extends Singer {
    private int prevPlayer;
    private float[] phonemes = new float[] {0f, 0.15f, 0f, 0.05f, 0.10f, 0.1f, 0.1f, 0.05f, 0.2f, 0.0f, 0.15f};

    public Singer1() {
        super();


        Tools.normalize(phonemes);
        String[] files = new String[] {"assets/vocal/zahSpectrum.wav","assets/vocal/wahSpectrum.wav",
                "assets/vocal/oohSpectrum.wav", "assets/vocal/piiSpectrum.wav",
                "assets/vocal/melSpectrum.wav", "assets/vocal/rohSpectrum.wav",//
                "assets/vocal/miSpectrum.wav", "assets/vocal/kaSpectrum.wav",
                "assets/vocal/leSpectrum.wav", "assets/vocal/maSpectrum.wav",//<-9
                "assets/vocal/ohSpectrum.wav"
        };



        try {
            for (int i = 0; i < files.length; i++) {
                SamplePlayer player = new SamplePlayer(ac, new Sample(files[i]));
                player.setKillOnEnd(false);
                players.add(player);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        pitchOffset = 0;
    }

    public void init() {
        if (!initialized) {
            super.init();

            for (int i = 0; i < players.size(); i++) {
                SamplePlayer player = players.get(i);

                Glide gainValue = new Glide(ac, 1, 200);
                gainValues.add(gainValue);
                Gain gain = new Gain(ac, 1, gainValue);
                gain.addInput(player);
                ac.out.addInput(gain);

                player.setToEnd();
                player.setLoopType(SamplePlayer.LoopType.NO_LOOP_FORWARDS);
                player.start();
            }

            /////

            //////

            ac.start();
        }
    }

    public void singKey(int key, int duration) {
        key = key + pitchOffset;

        int x = Tools.weightedRandom(phonemes);

        if (key == 39) {
            x = 1;
        } else if (key == 40) {
            x = 7;
        } else if (key == 44) {
            x = 3;
        } else if (key == 47) {
            x = 4;
        } else if (key == 51) {
            x = 5;
        } else if (key == 52) {
            x = 6;
        } else if (key == 56) {
            x = 8;
        }

        //say pizza
        if (prevPlayer == 3) {
            x = 0;
        } else if (prevPlayer == 7) {
            x = 9;
        }

        if (prevPlayer >= 0) {
            gainValues.get(prevPlayer).setValueImmediately(0);
        }

        SamplePlayer player = players.get(x);
        Glide gainValue = gainValues.get(x);
        gainValue.setValueImmediately(0);
       // gainValue.setGlideTime(duration);
        //gainValue.setGlideTime();
        int start = (4 + (key - 28)) * 2000 - 50;
        player.setPosition(start);
        player.setLoopStart(new Glide(ac, start + 200, 0));
        player.setLoopEnd(new Glide(ac, start + 700, 0));
        player.setLoopCrossFade(10);
        //player.setLoo
        player.setLoopType(SamplePlayer.LoopType.LOOP_FORWARDS);
        gainValue.setValue(1f);

        prevPlayer = x;
    }
}

