package com.nimus;

/**
 * Created by Sean on 6/20/2015.
 */
public class Conductor1 extends Conductor {
    CameraPanel cam;
    Singer singer;

    public Conductor1(Singer singer) {
        this.singer = singer;
    }

    public void start(CameraPanel cam) {
        this.cam = cam;

        singer.init();

        new Thread() {
            public void run() {
                singing = true;
                while (cam.timePercent < 0.9) {
                    float[] hsb = cam.needleHSB;
                    int key = (int) Tools.map(hsb[0], 0, 1, 60, 72);
                    System.out.println("SING: " + key);
                    singer.singKey(key, 1000);

                    try {
                        Thread.sleep(500);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                singer.shutup();
                singing = false;
                finished = true;
            }
        }.start();
    }
}
