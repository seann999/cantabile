package com.nimus;

/**
 * Created by Sean on 6/25/2015.
 */
public class Needle {
    private float needleX;
    private float width, height, totalTime, circleInnerRadius, circleOuterRadius;
    public float rotation = 0, timePercent = 0;
    public float forcedX, forcedY;

    public Needle(float width, float height, float totalTime, float circleInnerRadius, float circleOuterRadius) {
        this.width = width;
        this.height = height;
        this.totalTime = totalTime;
        this.circleInnerRadius = circleInnerRadius;
        this.circleOuterRadius = circleOuterRadius;
    }

    public void setNeedleTime(float time) {
       // System.out.println(time + " / " + totalTime);

        timePercent = time / totalTime;

        needleX = timePercent * circleInnerRadius + (1.0f - timePercent) * circleOuterRadius;
    }

    public int getNeedleX() {
        int mag = (int) ((needleX));
        //return (int) (((Math.cos(rotation) * mag) + width / 2));
        return (int) (width / 2 + mag);
    }

    public int getNeedleY() {
        //int mag = (int) ((needleX));
        //return (int) (((Math.sin(rotation) * mag) + height / 2));
        return (int) height / 2;
    }
}

