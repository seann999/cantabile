package com.nimus;

import com.nimus.classifier.ColorClassifier;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Sean on 6/20/2015.
 */
public class Conductor {
    public Singer singer;
    MidiCom midi = new MidiCom();
    public boolean singing = false;
    public boolean finished = false;

    public void start(CameraPanel cam) {
    }

    public void init(ColorClassifier cc) {}

    public void processImage(BufferedImage image, float radius) {
    }

    public Color classifyColor(Color color) {return Color.BLACK;}

    public void keyPressed(char key) {}

    public void stop() {}

    public void resetBeat() {
        midi.noteOn(-1);
    }
}
