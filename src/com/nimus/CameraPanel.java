package com.nimus;

import com.github.sarxos.webcam.Webcam;
import com.nimus.classifier.ColorClassifier;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by Sean on 6/7/2015.
 */
public class CameraPanel extends JPanel implements ActionListener {
    private final static String REPAINT = "repaint";

    private enum State {CALIBRATION, WAITING, RUNNING, FINISHED}

    ;

    public static String[] PRAISES = {
        "Fantastico!",
            "Romanzesco!",
            "Meraviglioso!",
            "Splendido!",
            "Stupendo!",
            "Incredibile!",
            "Bene!",
            "Bravo!",
            "Piacevole!",
            "Eccellente!",
            "Supremo!",
            "Bellissimo!",
            "Unico!",
            "Gustoso!",
            "Bello!",
            "Artistico!",
            "Magnifico!",
            "Superbo!",
            "Celeste!",
            "Divino!"
    };

    private Webcam webcam;
    private BufferedImage image;
    private BufferedImage edgeImage;

    private State state = State.CALIBRATION;
    private float scale = 3;
    private int width, height;
    private int circleOuterRadius, circleInnerRadius;

    private long timeStarted = -1; //milliseconds
    private float totalTime = 20;
    public float timePercent = 0; //seconds
    private int needleSize = 30;
    public Color needleColor = Color.BLACK;
    private boolean calibrationMode = false;
    //private float[] imgData = null;
    public float[] needleHSB = new float[3];

    private float[] analysisColor = new float[3];
    private float analysisComplexity, analysisVariation, analysisBrightness;

    public String key;
    private float confidence = 0;

    public static boolean STOP = false;

    private BasicStroke stroke = new BasicStroke(10);
    private BasicStroke thinStroke = new BasicStroke(4);
    private int fontSize = 40;
    private Font font = new Font("Arial", Font.PLAIN, fontSize);

    private float colorBiasStrength = 0.3f;
    private float anomalySensitivity = 10f;
    private CalibrationData calData = new CalibrationData();
    private boolean viewEdges = false;
    private boolean showInfo = true;

    private float[] finalDist;

    private float[] currentRGBData;
    private float[] currentHSBData = new float[3];
    private float[] prevHSBData = new float[3];
    private float currentComplexity;

    private int frame = 0;

    private Communicator comms;
    private boolean setupComms = false;

    private float anomalyDuration = 0;
    private float emptyDuration = 0;

    private float recognizeTime = 1f * 20.0f;
    private int rotateTime = 2000;
    private int rotateWaitTime = 3000;

    private boolean forceMotor = false;
    private boolean forceStop = false;

    private Conductor conductor;
    private int mouseX, mouseY;

    private BufferedImage savedImage;
    private Needle needle;
    private float rotationRate = 0.3f;
    private AffineTransform resetT = new AffineTransform();
    private ArrayList<Note> trail = new ArrayList<Note>();
    private float timeLapsed = 0;
    private Color trackColor = new Color(0, 255, 255, 100);
    private double trackStep = 1.0 / 30.0;

    private float idleDuration = 0;
    private float idleTotal = 3f;
    private float[] prevHSB = new float[4];

    private long lastFrame;
    private float delta;

    private ColorClassifier cc = new ColorClassifier();
    private BufferedImage cam;

    private boolean rotateImage = false;
    public static Fire FIRE;

    public static Color KEYCOLOR;

    public float changeSensitivity = 10;
    private boolean motorOn = false;
    private int fireIntensity = 0;

    //private Thread thread;

    public CameraPanel(Webcam webcam, Conductor conductor) {
        this.webcam = webcam;
        this.conductor = conductor;

        width = (int) (webcam.getImage().getWidth() * scale);
        height = (int) (webcam.getImage().getHeight() * scale);
        setPreferredSize(new Dimension(width, height));

        circleOuterRadius = (int) (height / 2.0f);
        circleInnerRadius = 0;//(int) (circleOuterRadius * 0.2f);

        initImage();

        comms = new Communicator();

        MidiCom midi = new MidiCom();

        Timer timer = new Timer(1000 / 30, this);
        timer.setActionCommand(REPAINT);
        timer.start();

        setFocusable(true);
        addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyChar() == 'c') {
                    calibrationMode = !calibrationMode;

                    if (calibrationMode) {
                        calData.reset();
                    }
                } else if (e.getKeyChar() == 'e') {
                    viewEdges = !viewEdges;
                } else if (e.getKeyChar() == 'w') {
                    anomalySensitivity++;
                } else if (e.getKeyChar() == 's') {
                    anomalySensitivity--;
                } else if (e.getKeyChar() == 'i') {
                    showInfo = !showInfo;
                } else if (e.getKeyChar() == 'n') {
                    colorBiasStrength = Math.min(colorBiasStrength + 0.1f, 1f);
                } else if (e.getKeyChar() == 'm') {
                    colorBiasStrength = Math.max(colorBiasStrength - 0.1f, 0f);
                } else if (e.getKeyChar() == 'v') {
                    setProgramState(State.WAITING);
                } else if (e.getKeyChar() == 'p') {
                    forceMotor = !forceMotor;
                } else if (e.getKeyChar() == 'o') {
                    forceStop = !forceStop;
                } else if (e.getKeyChar() == '9') {
                   saveSettings();
                } else if (e.getKeyChar() == '0') {
                    loadSettings();
                } else if (e.getKeyChar() == 'b') {
                    conductor.resetBeat();
                } else if (e.getKeyChar() == '-') {
                    changeSensitivity += 1;
                } else if (e.getKeyChar() == '=') {
                    changeSensitivity -= 1;
                } else if (e.getKeyChar() == 'r') {
                    System.out.println("RESET");
                    conductor.singer.shutup();
                    setProgramState(State.FINISHED);

                    readNeedleData();
                    conductor.stop();
                    setProgramState(State.WAITING);
                    //setProgramState(State.RUNNING);
                }

                conductor.keyPressed(e.getKeyChar());
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });

        conductor.init(cc);
        FIRE = new Fire();

        addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {
                int rgb = cam.getRGB((int) (mouseX / scale), (int) (mouseY / scale));

                cc.addData(Tools.rgbToHSB(rgb));
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                mouseX = e.getX();
                mouseY = e.getY();
            }
        });

        addKeyListener(cc);

        new Thread() {
            public void run() {
                while (!STOP) {
                    try {
                        int motor = 0;
                        if (forceStop) {
                            motor = 0;
                        } else if (forceMotor) {
                            motor = 10;
                        } else if (state == CameraPanel.State.RUNNING && motorOn) {
                            motor = 10;
                        } else {
                            motor = 0;
                        }
                        fireIntensity = FIRE.getIntensity();
                        //System.out.println("FIRE: " + fi);
                        int serv = (int) (timePercent * 120.0 + 30);
                        String msg = motor + "a" + serv + "a" + fireIntensity + "n";
                        //System.out.println(msg);
                        //comms.getSerial().writeInt(motor);
                        comms.getSerial().writeBytes(msg.getBytes());
                       // System.out.println("message sent.");

                        Thread.sleep(30);
                    } catch (Exception e) {
                        //e.printStackTrace();
                    }

                }
            }
        }.start();

        needle = new Needle(width, height, totalTime, circleInnerRadius, circleOuterRadius);
        setNeedleTime(0);
    }

    private void setProgramState(State state) {
        this.state = state;

        if (state == State.RUNNING) {
            conductor.singer.init();
            FIRE.fireOn();
            System.out.println("FIRE");
            conductor.singer.fire();


            //((Conductor2) conductor).applaud();

            analyzePizza();

            conductor.resetBeat();

            new Thread() {
                public void run() {
                    try {
                        Thread.sleep(rotateWaitTime);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    motorOn = true;

                    try {
                        Thread.sleep(rotateTime);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    startMusicThread();
                }
            }.start();
        } else if (state == State.FINISHED) {
            motorOn = false;
            FIRE.fireOff();

        } else if (state == State.WAITING) {
            reinitialize();


            conductor.finished = false;
        }
    }

    private void reinitialize() {
        trail.clear();
        needle.rotation = 0;
        System.out.println("RESETTING NEEDLE");
        timeStarted = -1;
        timeLapsed = 0;
        setNeedleTime(0);
        needle.forcedX = circleOuterRadius + width / 2;
        needle.forcedY = height / 2;
    }

    private BufferedImage deepCopy(BufferedImage bi) {
        ColorModel cm = bi.getColorModel();
        boolean isAlpha = cm.isAlphaPremultiplied();
        WritableRaster raster = bi.copyData(null);
        return new BufferedImage(cm, raster, isAlpha, null);
    }

    private String getPizzaName() {
        int i = 1;

        while (new File("assets/" + i + ".png").exists()) {
            i++;
        }

        return i + "";
    }

    private void analyzePizza() {
        needle.rotation = 0;

        try {
            Random r = new Random();
            //String name = "" + System.currentTimeMillis();
            String name = getPizzaName();
            File outputfile = new File("assets/" + name + ".png");
            System.out.println("save image as: " + outputfile.getName());
            //BufferedImage oImage = deepCopy(image);
            //BufferedImage tweet = oImage.getSubimage(image.getWidth() / 2 - image.getHeight() / 2, 0, image.getHeight(), image.getHeight());
//            ImageIO.write(tweet, "png", outputfile);
            ImageIO.write(image, "png", outputfile);
            savedImage = ImageIO.read(outputfile);

            //String[] cmd={"python",
              //      "C:/Users/Sean/Anaconda/Scripts/pizzaUploader.py",outputfile.getAbsolutePath()};
//           String[] cmd={"python",
//                    new File("assets/pizzaUploader.py").getAbsolutePath(),outputfile.getAbsolutePath(),
//            PRAISES[r.nextInt(PRAISES.length)]};
            String[] cmd={"python",
                    new File("assets/pizzaUploader.py").getAbsolutePath(),outputfile.getAbsolutePath(),
                    PRAISES[r.nextInt(PRAISES.length)] + " (#" + name + ")"};


           Runtime.getRuntime().exec(cmd);

//            analyzeTrack();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("ANALYZED");

        float[] d = ImageAnalyzer.analyzeColor(image, circleOuterRadius / scale);
        analysisVariation = d[0];
        analysisColor[0] = d[1];
        analysisColor[1] = d[2];
        analysisColor[2] = d[3];
        float[] hsb = new float[3];
        Color.RGBtoHSB((int) d[1], (int) d[2], (int) d[3], hsb);
        analysisComplexity = ImageAnalyzer.analyzeComplexity(image, null, circleOuterRadius / scale);
    }

    private void analyzeTrack() {
        Needle n = new Needle(width, height, totalTime, circleInnerRadius, circleOuterRadius);
        for (float i = 0; i < totalTime; i += trackStep) {
            n.rotation += rotationRate * trackStep / (1.0 / 30.0);
            n.setNeedleTime(i);
            Note note = new Note(n.getNeedleX(), n.getNeedleY());
            int x = (int) (note.x / scale);
            int y = (int) (note.y / scale);
            int rgb = savedImage.getRGB(x, y);
            int r = (rgb >> 16) & 0x000000FF;
            int g = (rgb >> 8) & 0x000000FF;
            int b = (rgb) & 0x000000FF;
            note.color = conductor.classifyColor(new Color(r, g, b));
            trail.add(note);
        }
    }

    private void startMusicThread() {
        readNeedleData();
        conductor.start(this);
    }

    private void initImage() {
        GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice device = env.getDefaultScreenDevice();
        GraphicsConfiguration config = device.getDefaultConfiguration();
        image = config.createCompatibleImage(webcam.getImage().getWidth(), webcam.getImage().getHeight(), Transparency.OPAQUE);

        edgeImage = config.createCompatibleImage(webcam.getImage().getWidth(), webcam.getImage().getHeight(), Transparency.OPAQUE);
    }

    public void drawTrack(Graphics2D g2d) {
        g2d.setColor(trackColor);
        //Point2D prev = null;
        float prevX = -1;
        float prevY = 0;
        g2d.setStroke(thinStroke);
        for (Note pt : trail) {
            if (prevX >= 0) {
                g2d.setColor(trackColor);
                g2d.drawLine((int) prevX, (int) prevY, (int) pt.x, (int) pt.y);
            }

            g2d.setColor(pt.color);
            g2d.fillRect((int) pt.x - 5, (int) pt.y - 5, 10, 10);

            prevX = pt.x;
            prevY = pt.y;
        }
    }

    public void paintComponent(Graphics g) {
        delta = (System.currentTimeMillis() - lastFrame) / 1000.0f;
        lastFrame = System.currentTimeMillis();

        if (!setupComms && getParent() != null) {
            ((JFrame) getParent().getParent().getParent()).addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    comms.close();
                }
            });
            setupComms = true;
        }

        cam = webcam.getImage();

        image.setData(cam.getData());

        BufferedImage mainImage = image;
        if (state == State.RUNNING && rotateImage) {
            mainImage.setData(savedImage.getData());
        }

        if (state == State.RUNNING) {
            if (conductor.singing) {
                updateNeedlePosition();
            } else if (conductor.finished) {
                setProgramState(State.FINISHED);
            }

            readNeedleData();
        }

        Graphics2D g2d = (Graphics2D) g;

        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, width, height);

        if (state == State.CALIBRATION || state == State.WAITING || calibrationMode || frame % 3 == 0 && state != State.RUNNING) {
            currentComplexity = ImageAnalyzer.analyzeComplexity(image, edgeImage, circleOuterRadius / scale);
            float[] colorData = ImageAnalyzer.analyzeColor(image, circleOuterRadius / scale);
            currentRGBData = Arrays.copyOfRange(colorData, 1, 4);
        }

        float anomalyValue = calcAnomaly(currentComplexity, currentRGBData);
        boolean anomaly = anomalyValue > anomalySensitivity;

        int mouseColor = image.getRGB((int) (mouseX / scale), (int) (mouseY / scale));

        Tools.rgbToHSB(currentRGBData, currentHSBData);
        currentHSBData[0] *= 255.0f;
        currentHSBData[1] *= 255.0f;
        //currentHSBData[2] *= 255.0f;
        currentHSBData[2] = currentComplexity;
        //System.out.println(Arrays.toString(currentHSBData));
        float change = Math.abs(prevHSB[0] - currentHSBData[0]) + Math.abs(prevHSB[1] - currentHSBData[1]) + Math.abs(prevHSB[2] - currentHSBData[2]);
        if (change < changeSensitivity) {
            idleDuration += delta;
        } else {
            idleDuration = 0;
        }

        //System.out.println("change: " + change);

        prevHSB[0] = currentHSBData[0];
        prevHSB[1] = currentHSBData[1];
        prevHSB[2] = currentHSBData[2];

        if (state == State.WAITING || state == State.CALIBRATION) {

            if (anomaly) {
                anomalyDuration++;

                if (anomalyDuration > recognizeTime && idleDuration >= idleTotal) {

                    if (state == State.WAITING) {
                        anomalyDuration = 0;
                        setProgramState(State.RUNNING);
                    }
                }
            } else {
                anomalyDuration = 0;
            }
        } else if (state == State.FINISHED) {
            if (!anomaly) {
                emptyDuration++;

                if (emptyDuration > recognizeTime && idleDuration >= idleTotal) {//approx 5 seconds
                    emptyDuration = 0;
                    setProgramState(State.WAITING);
                }
            } else {
                emptyDuration = 0;
            }
        }

        frame++;

        if (viewEdges) {
            g2d.drawImage(edgeImage, 0, 0, width, height, null);
        } else {
            if (rotateImage) {
                g2d.rotate(-needle.rotation, width / 2, height / 2);
            }

            conductor.processImage(mainImage, circleOuterRadius / scale);
            g2d.drawImage(mainImage, 0, 0, width, height, null);
            //g2d.setTransform(resetT);
        }

        //draw disc
        g2d.setStroke(stroke);
        if (calibrationMode) {
            calData.feed(currentComplexity, currentRGBData);
            g2d.setColor(Color.RED);
            g2d.drawOval(width / 2 - circleOuterRadius, height / 2 - circleOuterRadius, circleOuterRadius * 2, circleOuterRadius * 2);
        } else if (anomaly) {
            g2d.setColor(Color.PINK);
            g2d.drawOval(width / 2 - circleOuterRadius, height / 2 - circleOuterRadius, circleOuterRadius * 2, circleOuterRadius * 2);
           // System.out.println(anomalyDuration + " " + recognizeTime);
            int fill = (int) (((float) anomalyDuration / (float) recognizeTime) * 360.0);
            if (fill >= 360) {
                g2d.setColor(Color.CYAN);
            } else {
                g2d.setColor(Color.CYAN);
            }

            g2d.drawArc(width / 2 - circleOuterRadius, height / 2 - circleOuterRadius,
                    circleOuterRadius * 2, circleOuterRadius * 2, 90,
                    fill);
        } else {
            g2d.setColor(Color.GRAY);
            g2d.drawOval(width / 2 - circleOuterRadius, height / 2 - circleOuterRadius, circleOuterRadius * 2, circleOuterRadius * 2);
        }


        //g2d.drawOval(width / 2 - circleInnerRadius, height / 2 - circleInnerRadius, circleInnerRadius * 2, circleInnerRadius * 2);

        if (state == State.WAITING || state == State.FINISHED || state == State.CALIBRATION) {
            int fill = (int) ((idleDuration / idleTotal) * 360.0);
            if (fill >= 360) {
                g2d.setColor(Color.CYAN);
            } else {
                g2d.setColor(Color.CYAN);
            }

            int margin = 50;
            g2d.drawArc(width / 2 - circleOuterRadius + margin, height / 2 - circleOuterRadius + margin,
                    circleOuterRadius * 2 - margin * 2, circleOuterRadius * 2 - margin * 2, 90,
                    fill);
        }

        g2d.setColor(Color.PINK);
        g2d.drawLine(width / 2 - 30, height / 2, width / 2 + 30, height / 2);
        g2d.drawLine(width / 2, height / 2 - 30, width / 2, height / 2 + 30);

        if (state == State.RUNNING && rotateImage) {
            drawTrack(g2d);
        }

        //draw needle position
        g2d.setColor(Color.GRAY);
        if (rotateImage) {
            g2d.fillRect((int) needle.forcedX - needleSize / 2, (int) needle.forcedY - needleSize / 2, needleSize, needleSize);
        } else {
            g2d.fillRect((int) needle.getNeedleX() - needleSize / 2, (int) needle.getNeedleY() - needleSize / 2, needleSize, needleSize);
        }
        //g2d.setColor(Color.WHITE);
        //g2d.drawRect(needle.getNeedleX() - needleSize / 2 + 2, needle.getNeedleY() - needleSize / 2 + 2,
        //        needleSize / 2 - 4, needleSize / 2 - 4);

//        if (KEYCOLOR != null) {
//            g2d.setColor(KEYCOLOR);
//        }

        g2d.setColor(needleColor);
        g2d.fillRect(width - 200, height - 200, 200, 200);
        g2d.setColor(KEYCOLOR);
        g2d.fillRect(width - 400, height - 200, 200, 200);
      //`q  System.out.println(fireIntensity);
        g2d.setColor(new Color(fireIntensity, fireIntensity, fireIntensity));
        g2d.fillRect(width - 550, height - 150, 150, 150);

        g2d.setTransform(resetT);

        //write info
        g2d.setColor(Color.WHITE);
        g2d.setFont(font);
        int y = 1;
        int marginY = 5;



       // System.out.println(motor);


        if (showInfo) {
            g2d.drawString("STATE: " + getStateString(), 10, marginY + fontSize * y++);
            g2d.drawString(" ", 10, marginY + fontSize * y++);
            g2d.drawString("analyzed brightness: " + analysisBrightness, 10, marginY + fontSize * y++);
            g2d.drawString("analyzed color variation: " + analysisVariation, 10, marginY + fontSize * y++);
            g2d.drawString("analyzed complexity: " + analysisComplexity, 10, marginY + fontSize * y++);
            g2d.drawString("calibration complexity: " + calData.BGComplexityMean, 10, marginY + fontSize * y++);
            g2d.drawString("calibration average RGB: " + Arrays.toString(calData.BGColorMean), 10, marginY + fontSize * y++);
            //g2d.drawString("color H: " + needleHSB[0], 10, marginY + fontSize * y++);
            //g2d.drawString("key color: " + colorValue, 10, marginY + fontSize * y++);
            //g2d.drawString("note color bias: " + colorBiasStrength, 10, marginY + fontSize * y++);
            g2d.drawString("key: " + key + " confidence: " + confidence, 10, marginY + fontSize * y++);
            g2d.drawString("current complexity: " + (int) currentComplexity, 10, marginY + fontSize * y++);
            g2d.drawString("current average RGB: " + Arrays.toString(currentRGBData), 10, marginY + fontSize * y++);
            g2d.drawString("anomaly value: " + (int) anomalyValue, 10, marginY + fontSize * y++);
            g2d.drawString("anomaly sensitivity: " + (int) anomalySensitivity, 10, marginY + fontSize * y++);
            g2d.drawString("anomaly: " + anomaly, 10, marginY + fontSize * y++);
            g2d.drawString("change value: " + (int) change, 10, marginY + fontSize * y++);
            g2d.drawString("change sensitivity: " + (int) changeSensitivity, 10, marginY + fontSize * y++);
            g2d.drawString("force on: " + forceMotor + " force off: " + forceStop, 10, marginY + fontSize * y++);
            g2d.drawString("mouse color: " + Tools.rgbToHSBString(mouseColor), 10, marginY + fontSize * y++);
            g2d.drawString("toggle calibration [C]", 10, marginY + fontSize * y++);
            g2d.drawString("view edge detector [E]", 10, marginY + fontSize * y++);
            g2d.drawString("raise/lower anomaly sensitivity [W]/[S]", 10, marginY + fontSize * y++);
            g2d.drawString("raise/lower note color bias [N]/[M]", 10, marginY + fontSize * y++);
            g2d.drawString("show/hide info [I]", 10, marginY + fontSize * y++);
            g2d.drawString("motor start/stop [P]/[O]", 10, marginY + fontSize * y++);
            g2d.drawString("hard reset [R]", 10, marginY + fontSize * y++);
        }
    }

    private String getStateString() {
        if (state == State.CALIBRATION) {
            return state.toString() + " ([V] TO PROCEED)";
        } else if (state == State.WAITING) {
            return state.toString() + " (" + anomalyDuration + ")";
        } else if (state == State.FINISHED) {
            return state.toString() + " (" + emptyDuration + ")";
        }

        return state.toString();
    }

    private float calcAnomaly(float complexity, float[] color) {
        float colorDiff = (float) (Math.pow(color[1] - calData.BGColorMean[0], 2.0)
                + Math.pow(color[1] - calData.BGColorMean[1], 2.0)
                + Math.pow(color[2] - calData.BGColorMean[2], 2.0)) / 3.0f;
        float complexityDiff = (float) Math.pow(complexity - calData.BGComplexityMean, 2.0);

        return colorDiff / 50 + complexityDiff / 50;
    }

    private Color getNeedleColor() {
        int x = (int) (needle.getNeedleX() / scale);
        int y = (int) (image.getHeight() / 2);
        if (rotateImage) {
            x = (int) (needle.forcedX / scale);
            y = (int) (needle.forcedY / scale);
        }
        int rgb = 0;
        if (x >= 0 && y >= 0 && x < image.getWidth() && y < image.getHeight()) {
            if (state == State.RUNNING) {
               // x = (int) (needle.getNeedleX() / scale);
               // y = (int) (needle.getNeedleY() / scale);
                if (rotateImage) {
                    rgb = savedImage.getRGB(x, y);
                } else {
                    rgb = image.getRGB(x, y);
                }
            } else {
                rgb = image.getRGB(x, y);
            }
        }

        int r = (rgb >> 16) & 0x000000FF;
        int g = (rgb >> 8) & 0x000000FF;
        int b = (rgb) & 0x000000FF;

        return new Color(r, g, b);
    }

    private void updateNeedlePosition() {
        if (timeStarted < 0) {
            timeStarted = System.currentTimeMillis();
        }

        if (rotateImage) {
            timeLapsed += 1.0 / 30.0;
        } else {
            timeLapsed = (System.currentTimeMillis() - timeStarted) / 1000.0f;
        }

        needle.rotation += rotationRate;

        setNeedleTime(timeLapsed);

    }

    private void setNeedleTime(float time) {
        if (time >= totalTime) {
            conductor.stop();
        }

        //needle.setNeedleTime(time);
        if (rotateImage && trail != null && trail.size() > 0) {
            needle.forcedX = trail.get((int) (timeLapsed / trackStep)).x;
            needle.forcedY = trail.get((int) (timeLapsed / trackStep)).y;
        } else if (!rotateImage) {
            needle.setNeedleTime(time);
        }

        timePercent = time / totalTime;
        //System.out.println("t: " + timePercent);
    }

    private void readNeedleData() {
        needleColor = getNeedleColor();
        Color.RGBtoHSB(needleColor.getRed(), needleColor.getGreen(), needleColor.getBlue(), needleHSB);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(REPAINT)) {
            repaint();
        }
    }

    public void saveSettings() {
        String file = "assets/calData.txt";
        System.out.println("SAVING MAIN DATA.");
        try {
            PrintWriter writer = new PrintWriter(new FileWriter(new File(file)));
            writer.println(calData.BGColorMean[0]);
            writer.println(calData.BGColorMean[1]);
            writer.println(calData.BGColorMean[2]);
            writer.println(calData.BGComplexityMean);
            writer.println(anomalySensitivity);
            writer.println(changeSensitivity);
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadSettings() {
        System.out.println("LOADING MAIN DATA.");
        String file = "assets/calData.txt";
        try {
            BufferedReader reader = new BufferedReader(new FileReader(new File(file)));

            calData.BGColorMean[0] = Float.parseFloat(reader.readLine());
            calData.BGColorMean[1] = Float.parseFloat(reader.readLine());
            calData.BGColorMean[2] = Float.parseFloat(reader.readLine());
            calData.BGComplexityMean = Float.parseFloat(reader.readLine());
            anomalySensitivity = Float.parseFloat(reader.readLine());
            changeSensitivity = Float.parseFloat(reader.readLine());
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
