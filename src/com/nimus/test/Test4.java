package com.nimus.test;

import org.opencv.core.*;
import org.opencv.features2d.FeatureDetector;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sean on 6/24/2015.
 */

public class Test4 {

    public static void dilate(int dilate, Mat mat) {
        Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new  Size(2*dilate + 1, 2*dilate+1));
        Imgproc.dilate(mat, mat, element);
    }

    public static void erode(int erode, Mat mat) {
        Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new  Size(2*erode + 1, 2*erode+1));
        Imgproc.erode(mat, mat, element);
    }

    public static void main(String[] args) {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        String sourcePath = "assets/Untitled67.png";

        Mat srcImgMat = Imgcodecs.imread(sourcePath);

        Imgproc.cvtColor(srcImgMat,srcImgMat,Imgproc.COLOR_BGR2HSV);

        Mat mask = new Mat(srcImgMat.size(), CvType.CV_32F);
        Core.inRange(srcImgMat, new Scalar(50, 128, 0), new Scalar(80, 255, 255), mask);

        //erode(10, mask);

        //leaves
        dilate(20, mask);
        erode(70, mask);
        dilate(60, mask);
        ////////////

        Imgcodecs.imwrite("assets/poop.png", mask);

        Mat overlap = Mat.zeros(mask.size(), mask.type());

        //====================
        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Imgproc.findContours(mask, contours, new Mat(), Imgproc.RETR_LIST,Imgproc.CHAIN_APPROX_SIMPLE);
        System.out.println("contours: " + contours.size());
        //====================

        Imgproc.drawContours(mask, contours, -1, new Scalar(255, 255, 255), -1);//overwrites
        Imgcodecs.imwrite("assets/poop2.png", mask);

        //===============

        MatOfKeyPoint matOfKeyPoints = new MatOfKeyPoint();
        FeatureDetector blobDetector = FeatureDetector.create(FeatureDetector.SIMPLEBLOB);
        blobDetector.read("assets/basil.txt");
        blobDetector.detect(mask, matOfKeyPoints);
        System.out.println("Detected " + matOfKeyPoints.size() + " blobs in the image");
        List<KeyPoint> pts = matOfKeyPoints.toList();
        Mat blobs = new Mat(mask.size(), mask.type());
        for (KeyPoint pt : pts) {
            Imgproc.rectangle(blobs, new Point(pt.pt.x - pt.size / 2, pt.pt.y - pt.size / 2),
                    new Point(pt.pt.x + pt.size / 2, pt.pt.y + pt.size / 2), new Scalar(255, 255, 128), -1);
        }
        Imgcodecs.imwrite("assets/blobs.png", blobs);
        Mat fin = new Mat(mask.size(), mask.type());
//        Core.invert(blobs, blobs);
        Core.subtract(Mat.ones(blobs.size(), blobs.type()), blobs, blobs);
        mask.copyTo(fin, blobs);
        //==================

        Imgcodecs.imwrite("assets/contours.png", fin);

        Mat fag = new Mat();
        srcImgMat.copyTo(fag, fin);
        Imgproc.cvtColor(fag,fag,Imgproc.COLOR_HSV2BGR);
        Imgcodecs.imwrite("assets/final.png", fag);
    }

}
