package com.nimus.test;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sean on 6/24/2015.
 */

public class CVTest3 {


    public static void main(String[] args) {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        String sourcePath = "assets/Untitled67.png";

        Mat srcImgMat = Imgcodecs.imread(sourcePath);

        if (srcImgMat == null) {
            System.out.println("Failed to load image at " + sourcePath);
            return;
        }

        System.out.println("Loaded image at " + sourcePath);

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Mat hierarchy = new Mat();
        Mat dest = new Mat();
        Imgproc.cvtColor(srcImgMat, dest, Imgproc.COLOR_RGB2GRAY);
        // Mat dest = new Mat(src.width(), src.height(), src.type());
        Core.inRange(dest, new Scalar(58,125,0), new Scalar(256,256,256), dest);
//        Imgproc.findContours(srcImgMat, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);
        Imgproc.findContours(dest, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
        Imgproc.drawContours(dest, contours, -1, new Scalar(255,255,0));
     //   Imgproc.drawContours(srcImgMat, contours, -1, new Scalar(255, 255, 255), 1);
//        for (KeyPoint pt : keyPoints) {
//            System.out.println("kk");
//            Imgproc.rectangle(srcImgMat, new Point(pt.pt.x, pt.pt.y),
//                    new Point(100 + pt.pt.x, pt.pt.y + 100), new Scalar(0, 255, 0));
//        }
        String filename = "assets/blobs.png";

        Imgcodecs.imwrite(filename, dest);
    }

}
