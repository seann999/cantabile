package com.nimus.test;

import org.opencv.core.*;
import org.opencv.features2d.FeatureDetector;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sean on 6/24/2015.
 */

public class CVTest2 {


    public static void main(String[] args) {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        String sourcePath = "assets/Untitled67.png";

        Mat srcImgMat = Imgcodecs.imread(sourcePath);

        if (srcImgMat == null) {
            System.out.println("Failed to load image at " + sourcePath);
            return;
        }

        System.out.println("Loaded image at " + sourcePath);

        MatOfKeyPoint matOfKeyPoints = new MatOfKeyPoint();

        //  File tempDir = context.getCacheDir();

        FeatureDetector blobDetector = FeatureDetector.create(FeatureDetector.SIMPLEBLOB);

        File tempFile = null;
        try {
            tempFile = File.createTempFile("config", ".yml");


            String settings = "%YAML:1.0\nminArea: 0\nblobColor: 0\n";

            FileWriter writer = new FileWriter(tempFile, false);
            writer.write(settings);
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Imgproc.cvtColor(srcImgMat,srcImgMat,Imgproc.COLOR_RGB2HSV);
        Mat copy = srcImgMat.clone();

        Core.inRange(srcImgMat, new Scalar(0, 0, 0), new Scalar(255, 255, 255), copy);
        Imgcodecs.imwrite("assets/srcImgMat.png", copy);
        int erosion_size = 10;
        Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new  Size(2*erosion_size + 1, 2*erosion_size+1));

       // Imgproc.erode(copy, copy, element);
        //int erosion_size = 10;
        //Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new  Size(2*erosion_size + 1, 2*erosion_size+1));
       // Imgproc.dilate(copy, copy, element);
        Mat overlap = Mat.zeros(copy.size(), copy.type());

        //====================
        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Imgproc.findContours(copy, contours, new Mat(), Imgproc.RETR_LIST,Imgproc.CHAIN_APPROX_SIMPLE);

        //Imgproc.drawContours(imageBlurr, contours, 1, new Scalar(0,0,255));
        //for(int i=0; i< contours.size();i++){
            System.out.println("contours: " + contours.size());
           // if (Imgproc.contourArea(contours.get(i)) > 50 ){
                Imgproc.drawContours(overlap, contours, -1, new Scalar(255, 255, 255));

            //}
        //}
        //====================


        blobDetector.read("assets/corn.txt");

        // blobDetector.
        blobDetector.detect(copy, matOfKeyPoints);

        System.out.println("Detected " + matOfKeyPoints.size() + " blobs in the image");

        List<KeyPoint> keyPoints = matOfKeyPoints.toList();
        ArrayList<MatOfPoint> pts = new ArrayList<MatOfPoint>();
//        for (KeyPoint key : keyPoints) {
//            System.out.println("kk");
//            pts.add(new MatOfPoint(key.pt));
//            Imgproc.rectangle(srcImgMat, new Point(key.pt.x - key.size / 2, key.pt.y - key.size / 2),
//
//                           new Point(key.pt.x + key.size, key.pt.y + key.size), new Scalar(255, 255, 255), 10);
//            Imgproc.rectangle(overlap, new Point(key.pt.x - key.size / 2, key.pt.y - key.size / 2),
//
//                    new Point(key.pt.x + key.size, key.pt.y + key.size), new Scalar(255, 255, 255), 10);
//
//        }
     //   Features2d.drawKeypoints(overlap, matOfKeyPoints, overlap);
      //  for (KeyPoint pt : keyPoints) {
          //
           // Imgproc.drawContours(srcImgMat, pts, pts.size() - 1, new Scalar(0, 255, 0));
            //Imgproc.contourArea(pt);

            // Imgproc.rectangle(srcImgMat, new Point(pt.pt.x, pt.pt.y),
            //       new Point(100 + pt.pt.x, pt.pt.y + 100), new Scalar(0, 255, 0));
      //  }
        String filename = "assets/blobs.png";

        Imgcodecs.imwrite(filename, srcImgMat);
        Mat fag = new Mat();
        srcImgMat.copyTo(fag, overlap);

        Imgcodecs.imwrite("assets/bl2.png", fag);


        blobDetector.write("assets/corn.txt");
    }

}
