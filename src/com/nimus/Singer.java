package com.nimus;

import net.beadsproject.beads.core.AudioContext;
import net.beadsproject.beads.data.Sample;
import net.beadsproject.beads.ugens.Gain;
import net.beadsproject.beads.ugens.Glide;
import net.beadsproject.beads.ugens.SamplePlayer;

import java.util.ArrayList;

/**
 * Created by Sean on 6/20/2015.
 */
public class Singer {
    protected AudioContext ac;
    public int pitchOffset = -12;
    protected ArrayList<SamplePlayer> players = new ArrayList<SamplePlayer>();
    protected ArrayList<Glide> gainValues = new ArrayList<Glide>();
    private SamplePlayer applause;
    private SamplePlayer fire;
    Glide fxValue;
    boolean initialized = false;

    public Singer() {
        ac = new AudioContext();
        try {
            applause = new SamplePlayer(ac, new Sample("assets/applause.wav"));
            applause.setKillOnEnd(false);
            fire = new SamplePlayer(ac, new Sample("assets/fire.wav"));
            fire.setKillOnEnd(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        applause.setToEnd();
        fire.setToEnd();
        fxValue = new Glide(ac, 1, 0);
    }

    public void init() {
        if (!initialized) {
            Gain gain2 = new Gain(ac, 1, fxValue);
            gain2.addInput(fire);
            ac.out.addInput(gain2);
            fire.setToEnd();
            fire.setLoopType(SamplePlayer.LoopType.NO_LOOP_FORWARDS);
            initialized = true;

            Gain gain = new Gain(ac, 1, fxValue);
            gain.addInput(applause);
            ac.out.addInput(gain);
            applause.setToEnd();
            applause.setLoopType(SamplePlayer.LoopType.NO_LOOP_FORWARDS);


        }
    }

    public void singKey(int key, int duration) {}

    public void pause(float time) {
        for (Glide gain : gainValues) {
            gain.setGlideTime(time);
            gain.setValue(0);
        }
    }

    public void applaud(float volume) {
        fxValue.setValueImmediately(volume);
        applause.setToLoopStart();
        applause.start();

        //new Exception().printStackTrace();
    }

    public void fire() {
       // applaud();
//        fxValue.setValueImmediately(5);
//        fire.setToLoopStart();
//        fire.start();
        fxValue.setValueImmediately(1);
        fire.setToLoopStart();
        fire.start();
    }

    public void shutup() {
        for (Glide gain : gainValues) {
            gain.setValueImmediately(0);
        }
    }
}
